const config = require('../config.json');
const helper = require('../helper.js');

module.exports = {
    message: async obj => {
        let { msg } = obj;

        if(['jams'].includes(msg.channel.name)){
            if(
                (msg.content.includes('https://') || msg.content.includes('http://')) &&
                (msg.content.includes('youtube.com') || msg.content.includes('youtu.be') || msg.content.includes('soundcloud.com') || msg.content.includes('spotify.com'))
            ){
                await msg.react(msg.guild.emojis.cache.get('791080660903264256'));
                await msg.react(msg.guild.emojis.cache.get('699350037733441696'));
                await msg.react(msg.guild.emojis.cache.get('684444498297749535'));
            }
        }
    }
};
