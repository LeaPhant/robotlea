const config = require('../config.json');
const helper = require('../helper.js');

module.exports = {
    message: async obj => {
        let { msg } = obj;

        const msgL = msg.content.toLowerCase();

        if(msgL.includes('<@&843340331041685564>')){
            await msg.member.roles.add('843340331041685564');
        }else if(
            ['<@!692675314962268180>', '<@692675314962268180>'].some(a => msgL.includes(a)) &&
            ['wemove', 'remove'].some(a => msgL.includes(a)) &&
            msgL.includes('h')
        ){
            if(['please', 'pwease', 'pls', 'plz'].some(a => msgL.includes(a))){
                if(msg.member.roles.cache.has('843340331041685564')){
                    await msg.member.roles.remove('843340331041685564');
                    await msg.reply("okay, ive wemoved h role fwom u but pwease be mowre caweful next time (≧◡≦)");
                }else{
                    await msg.reply("it doesnt seem like u have h role, what do yuw want me to wemove? <:Think:790399525495898122>");
                }
            }else{
                await msg.reply("could u be a bit nicer pwease ( ⌣̀_⌣́)");
            }
        }
    }
};
