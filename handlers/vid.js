const config = require('../config.json');
const helper = require('../helper.js');
const util = require('util');
const fs = require('fs').promises;
const axios = require('axios');
const { DateTime } = require("luxon");
const { inlineCode } = require('@discordjs/builders');
const { ActionRowBuilder, ButtonBuilder, ButtonStyle } = require('discord.js');

const { createCanvas, loadImage, GlobalFonts } = require('@napi-rs/canvas');
const { drawText } = require('canvas-txt');

const path = require('path');
const child_process = require('child_process');

const execFile = util.promisify(child_process.execFile);
const exec = util.promisify(child_process.exec);

GlobalFonts.registerFromPath(path.join(__dirname, 'MuskDontSueMyAss.woff2'), 'MuskyRegular');

const ffmpeg = config.ffmpeg_path ?? 'ffmpeg';

// stolen from https://stackoverflow.com/a/19593950 :3 
const roundedCorners = (ctx, x, y, width, height, radius) => {
    ctx.beginPath();
    ctx.moveTo(x + radius, y);
    ctx.lineTo(x + width - radius, y);
    ctx.quadraticCurveTo(x + width, y, x + width, y + radius);
    ctx.lineTo(x + width, y + height - radius);
    ctx.quadraticCurveTo(x + width, y + height, x + width - radius, y + height);
    ctx.lineTo(x + radius, y + height);
    ctx.quadraticCurveTo(x, y + height, x, y + height - radius);
    ctx.lineTo(x, y + radius);
    ctx.quadraticCurveTo(x, y, x + radius, y);
    ctx.closePath();
};

const TWEET_WIDTH = 520;
const TWEET_PADDING = 15;

const PADDED_WIDTH = TWEET_WIDTH - TWEET_PADDING * 2;

const renderTweet = async url => {
    let id;

    try {
        const tweetUrl = new URL(url);
        id = path.basename(tweetUrl.pathname);
    } catch(e) {
        console.error(e);
        return;
    }

    const { tweet } = await fetch(`https://api.fxtwitter.com/status/${id}`, {
        headers: {
            'User-Agent': 'robotlea tweet embed (gitlab.com/leaphant/robotlea)'
        }
    }).then(response => { return response.json() });

    //const tweet = {"url":"https://twitter.com/GoodReddit/status/1827959057002664052","id":"1827959057002664052","text":"","author":{"id":"1313147693628297218","name":"good reddit","screen_name":"GoodReddit","avatar_url":"https://pbs.twimg.com/profile_images/1620603265804107777/gF_xMz6t_200x200.jpg","banner_url":"https://pbs.twimg.com/profile_banners/1313147693628297218/1713502891","description":"owned by a bag of marbles //\n\nsometimes bad or questionable reddit //\ndm for submissions","location":"","url":"https://twitter.com/GoodReddit","followers":1280492,"following":140,"joined":"Mon Oct 05 16:03:07 +0000 2020","likes":2351,"website":null,"tweets":10954,"avatar_color":null},"replies":6,"retweets":245,"likes":3210,"created_at":"Mon Aug 26 06:39:35 +0000 2024","created_timestamp":1724654375,"possibly_sensitive":false,"views":57087,"is_note_tweet":false,"community_note":null,"lang":"zxx","replying_to":null,"replying_to_status":null,"media":{"all":[{"type":"photo","url":"https://pbs.twimg.com/media/GV43D-XXMAASdX1.jpg","width":720,"height":930,"altText":""}],"photos":[{"type":"photo","url":"https://pbs.twimg.com/media/GV43D-XXMAASdX1.jpg","width":720,"height":930,"altText":""}]},"source":"Twitter Web App","twitter_card":"summary_large_image","color":null};

    const canvas = createCanvas(TWEET_WIDTH, 1000);
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = 'black';
    ctx.fillRect(0, 0, TWEET_WIDTH, 1000);

    const avatarPromise = loadImage(tweet.author.avatar_url);
    const photoPromises = [];
    // this is an array purely for convenience it will always be one element lol
    const quoteAvatarPromise = [];

    if (tweet.media?.photos?.length > 0) {
        for (const photo of tweet.media.photos) {
            photoPromises.push(loadImage(photo.url));
        }
    }

    if (tweet.quote?.author?.avatar_url) {
        quoteAvatarPromise.push(loadImage(tweet.quote.author.avatar_url));
    }

    const photoResponses = await Promise.all([avatarPromise, ...photoPromises]);

    const avatar = photoResponses[0];
    let offset = 1;

    const photos = photoPromises.length > 0 ? photoResponses.slice(offset, offset + photoPromises.length) : [];
    offset += photoPromises.length;

    const quoteAvatar = quoteAvatarPromise.length > 0 ? photoResponses.slice(offset, offset + 1) : null;
    offset += 1;

    ctx.save();
    roundedCorners(ctx, TWEET_PADDING, TWEET_PADDING, 40, 40, 20);
    ctx.clip();

    ctx.drawImage(avatar, TWEET_PADDING, TWEET_PADDING, 40, 40);

    ctx.restore();

    ctx.fillStyle = 'white';

    const textFormat = {
        align: 'left',
        vAlign: 'top',
        font: 'MuskyRegular',
        x: 15,
        width: PADDED_WIDTH
    };

    drawText(ctx, tweet.author.name, {
        ...textFormat,
        x: TWEET_PADDING + 40 + 8,
        y: TWEET_PADDING + 5,
        fontSize: 15,
        width: TWEET_WIDTH - TWEET_PADDING - 45 - 8,
        height: 40 - 8
    });

    ctx.fillStyle = '#71767B';

    drawText(ctx, `@${tweet.author.screen_name}`, {
        ...textFormat,
        x: TWEET_PADDING + 40 + 8,
        y: 15 + 5,
        fontSize: 15,
        vAlign: 'bottom',
        width: TWEET_WIDTH - TWEET_PADDING - 45 - 8,
        height: 40 - 8
    });

    ctx.fillStyle = 'white';
    
    let currentOffset = 70;

    if (tweet.text) {
        const { height } = drawText(ctx, tweet.text, {
            ...textFormat,
            y: currentOffset,
            width: PADDED_WIDTH,
            fontSize: 17
        });

        currentOffset += Math.ceil(height) + TWEET_PADDING;
    }

    if (photos.length == 1) {
        const photo = photos[0];

        const ratio = photo.height / photo.width;

        const x = TWEET_PADDING, y = currentOffset, w = PADDED_WIDTH, h = w * ratio;

        ctx.strokeStyle = '#2f3336';
        ctx.lineWidth = 1.5;

        ctx.save();
        roundedCorners(ctx, x, y, w, h, TWEET_PADDING + 1);
        ctx.stroke();
        ctx.clip();
        ctx.drawImage(photo, x, y, w, h);
        ctx.restore();

        currentOffset += Math.ceil(h);
    }

    const date = DateTime.fromSeconds(tweet.created_timestamp);
    const formattedDate = date.toFormat("h:mm a '·' DD");

    ctx.fillStyle = '#71767B';

    currentOffset += TWEET_PADDING + 5;

    const { height } = drawText(ctx, formattedDate, {
        ...textFormat,
        y: currentOffset,
        fontSize: 15
    });

    currentOffset += Math.ceil(height);

    // final height
    currentOffset += TWEET_PADDING;

    const outputCanvas = createCanvas(TWEET_WIDTH, currentOffset);
    const outputCtx = outputCanvas.getContext('2d');

    roundedCorners(outputCtx, 0, 0, TWEET_WIDTH, currentOffset, 15);
    outputCtx.clip();
    outputCtx.drawImage(canvas, 0, 0);

    const buf = outputCanvas.toBuffer('image/png')
    await fs.writeFile('./tweet-test.png', buf);
};

module.exports = {
    message: async obj => {
        let { msg } = obj;

        const argv = msg.content.split(" ");

        let UPLOAD_LIMIT = 10 * 1000 * 1000;

        if (msg.guild !== null) {
            const serverBoosts = msg.guild.premiumSubscriptionCount;

            if (serverBoosts >= 14)
                UPLOAD_LIMIT = 100 * 1000 * 1000;
            else if (serverBoosts >= 7)
                UPLOAD_LIMIT = 50 * 1000 * 1000;
        }

        const sites = [
            "tiktok.com",
            "https://redd.it",
            "https://v.redd.it",
            "reddit.com",
            "https://t.co",
            "facebook.com",
            "instagram.com",
            "nicovideo.jp/watch",
            "https://twitter.com",
            "https://x.com",
            "https://mobile.twitter.com",
            "xhslink.com",
            ...(config?.vidSites ?? [])
        ];

        const sitesDelEmbed = [
            "https://redd.it",
            "https://v.redd.it",
            "reddit.com",
            "https://twitter.com",
            "https://x.com",
            "https://mobile.twitter.com",
            "https://vm.tiktok.com",
            "https://vt.tiktok.com",
            "https://www.tiktok.com",
            "https://tiktok.com",
            "instagram.com",
            "xhslink.com",
            ...(config?.vidSitesDelEmbed ?? [])
        ];

        for (let arg of argv) {
            if (!arg.startsWith('https://') && !arg.startsWith('http://') || !sites.some(a => arg.includes(a)) || arg.includes('.mp4'))
                continue;

            arg = arg.replaceAll('fxtwitter.com', 'twitter.com');
            arg = arg.replaceAll('x.com', 'twitter.com');
            arg = arg.replaceAll('fixupx.com', 'twitter.com');
            arg = arg.replaceAll('vxtwitter.com', 'twitter.com');
            arg = arg.replaceAll('fixvx.com', 'twitter.com');

            if (arg.includes('tiktok.com/t/'))
                arg = arg.replaceAll('www.tiktok.com', 'vm.tiktok.com');

            msg.react('705628243830636574').catch(console.error);

            let dlPath = './downloads/';

            const ytdlpArgs = [
                '--no-simulate',
                '--dump-json',
                '-I', '1',
                '-o', `${dlPath}%(id)s.%(ext)s`,
                '-S', 'codec:h264',
                arg
            ];

            if (arg.includes('//youtube.com/') || arg.includes('//youtu.be/')) {
                ytdlpArgs.unshift('--username', 'oauth2', '--password', "''");
            }

            let uploadPath;
            let ytdlpOutput, info = {};

            try {
                ytdlpOutput = await execFile('yt-dlp', ytdlpArgs);
                const infoJson = ytdlpOutput.stdout.trim();
                info = JSON.parse(infoJson);
            } catch(e) {
                if (!arg.includes('tiktok.com/')) {
                    msg.reactions.cache.find(a => a.me).remove().catch(console.error);
                    return;
                }

                info = {
                    id: Date.now(),
                    ext: 'mp4',
                    extractor: 'TikTok',
                    width: 0,
                    height: 0
                };
            }

            const targetFileName = `${info.id}.${info.ext}`;
            let fileName = targetFileName;
            dlPath += targetFileName;
            uploadPath = dlPath;

            if (argv.includes('-an')) {
                const ffArgs = [
                    '-y',
                    '-i', dlPath,
                    '-c', 'copy',
                    '-movflags', '+faststart'
                ];

                if (argv.includes('-an')) {
                    ffArgs.push('-an');
                }

                uploadPath = dlPath.replace(path.extname(dlPath), 'p' + path.extname(dlPath));

                ffArgs.push(uploadPath);

                await execFile(ffmpeg, ffArgs)
            }

            let row;

            if (info.extractor == 'TikTok' &&
                (['.mp3', '.m4a'].includes(path.extname(dlPath))
                || info.width == 0 && info.height == 0)
            ) {
                const response = await axios.post('http://localhost:9000/', { url: arg }, { 
                    headers: { 
                        'Accept': 'application/json' 
                    }
                });

                const audioRequest = axios.post('http://localhost:9000/', { url: arg, downloadMode: 'audio' }, { 
                    headers: { 
                        'Accept': 'application/json' 
                    }
                });

                const images = response.data.picker;

                if (images == null || images.length == 0) {
                    return;
                }

                const imgDlPath = `./downloads/${info.id}`;
                const imgDlRsPath = `./downloads/${info.id}rs`;
                
                await fs.mkdir(imgDlPath, { recursive: true });
                await fs.mkdir(imgDlRsPath, { recursive: true });

                const downloadImages = [];
                const imagePaths = [];
                const imageRsPaths = [];

                for (const [index, imageUrl] of images.map(a => a.url).entries()) {
                    const url = new URL(imageUrl);
                    const imageName = `${index}${path.extname(url.pathname)}`;
                    imagePaths.push(`${imgDlPath}/${imageName}`);
                    imageRsPaths.push(`${imgDlRsPath}/${imageName}`);
                    downloadImages.push(execFile('curl', ['-o', `${imgDlPath}/${imageName}`, url]));
                }

                await Promise.all(downloadImages);

                if (imagePaths.length == 1) {
                    uploadPath = imagePaths[0];
                    fileName = path.basename(fileName, '.mp4') + '.jpg';

                    if (response.data.audio) {
                        const imagePath = uploadPath.replace(path.extname(uploadPath), '') + '.mp4';
                        fileName = path.basename(imagePath);

                        const audioResponse = await audioRequest;

                        await execFile(ffmpeg, ['-loop', '1', '-i', imagePaths[0], '-i', `"${audioResponse.data.url}"`, '-shortest', '-c:v', 'libx264', '-preset', 'ultrafast', '-tune', 'stillimage', imagePath], { shell: true });
                        uploadPath = imagePath;
                    }
                } else {
                    let maxWidth = 0, maxHeight = 0;

                    for (const image of imagePaths) {
                        const { stdout } = await execFile('identify', ['-format', '%wx%h', image]);
                        const [width, height] = stdout.split('x');

                        maxWidth = Math.max(maxWidth, width);
                        maxHeight = Math.max(maxHeight, height);
                    }

                    if (maxWidth == 0 || maxHeight == 0) {
                        msg.reactions.cache.find(a => a.me).remove().catch(console.error);
                        return;
                    }

                    if (maxWidth % 2 != 0) maxWidth += 1;
                    if (maxHeight % 2 != 0) maxHeight += 1;

                    await execFile('mogrify', ['-resize', `${maxWidth}x${maxHeight}`, '-gravity', 'center', '-background', 'black', '-extent', `${maxWidth}x${maxHeight}`, '-path', imgDlRsPath, `${imgDlPath}/*`]);

                    const slideshowPath = uploadPath.replace(path.extname(uploadPath), '') + '.mp4';
                    fileName = path.basename(slideshowPath);

                    const slideshowArgs = [];
                    let filterComplex = '';

                    for (const image of imageRsPaths) {
                        slideshowArgs.push('-loop', 1, '-t', 2.925, '-i', image);
                    }

                    for (let i = 0; i < imageRsPaths.length - 1; i++) {
                        if (i == 0) {
                            filterComplex += '[0][1]';
                        } else {
                            filterComplex += `[f${i}][${i + 1}]`;
                        }

                        filterComplex += `xfade=transition=slideleft:duration=0.35:offset=${2.575 * (i + 1)}`;
                        
                        if (i < imagePaths.length - 2) {
                            filterComplex += `[f${i+1}];`;
                        } else {
                            filterComplex += ',format=yuv420p[v]';
                        }
                    }

                    if (response.data.audio) {
                        const audioResponse = await audioRequest;
                        slideshowArgs.push('-stream_loop', '-1', '-i', `"${audioResponse.data.url}"`, '-shortest');
                    }

                    slideshowArgs.push('-filter_complex', `"${filterComplex}"`, 
                    '-map', '"[v]"', '-map', `${imageRsPaths.length}:a`,
                    '-c:v', 'libx264', '-preset', 'veryfast', '-tune', 'stillimage', '-r', 30,
                    '-c:a', 'libopus', '-b:a', '96k', '-y', slideshowPath);

                    await execFile(ffmpeg, slideshowArgs, { shell: true });
                    uploadPath = slideshowPath;

                    row = new ActionRowBuilder()
                    .addComponents(
                        new ButtonBuilder()
                            .setCustomId(`get-images-${info.id}`)
                            .setLabel(`${imagePaths.length} Images`)
                            .setEmoji('🖼️')
                            .setStyle(ButtonStyle.Primary),
                    );
                }
            }

            const embeds = [];

            let content;

            if (sitesDelEmbed.some(a => arg.includes(a))) {
                switch(info.extractor) {
                    case 'Reddit':
                        const sub = info.webpage_url.split("/").slice(3, 5).join('/');
                        content = `${inlineCode(sub)} – ${inlineCode(info.fulltitle)}`;
                        break;
                    case 'TikTok':
                        const desc = info.description?.split(" ");
                        const filtered_desc = desc?.filter((a, i) => i == 0 || a.trim().charAt('0') != '#');

                        if (filtered_desc) {
                            content = `${inlineCode('@' + info.uploader)} - ${inlineCode(filtered_desc.join(' '))}`;
                        }
                        break;
                    case 'twitter':
                        let tweet = info.description.split(" ");
                        tweet.pop();

                        const tweet_text = tweet.join(' ');

                        /*
                        embeds.push({
                            color: 0x1da1f2,
                            author: {
                                url: info.uploader_url,
                                name: `${info.uploader} (@${info.uploader_id})`
                            },
                            description: tweet,
                            footer: {
                                icon_url: 'https://abs.twimg.com/icons/apple-touch-icon-192x192.png',
                                text: 'Twitter'
                            },
                            fields: [{
                                name: 'Retweets',
                                value: info.repost_count.toString(),
                                inline: true
                            }, {
                                name: 'Likes',
                                value: info.like_count.toString(),
                                inline: true
                            }, {
                                name: 'Replies',
                                value: info.comment_count.toString(),
                                inline: true
                            }],
                            timestamp: info.timestamp * 1000
                        });*/

                        if (tweet.length > 0) {
                            content = `${inlineCode('@' + info.uploader_id)} - ${inlineCode(tweet_text)}`;
                        }
                        break;
                    case 'html5':
                    case 'generic':
                    case 'youtube':
                        msg.reactions.cache.find(a => a.me).remove().catch(console.error);
                        return;
                    case 'XiaoHongShu':
                        content = inlineCode(info.fulltitle);
                        break;
                    default:
                        content = `${inlineCode(info.uploader)} – ${inlineCode(info.fulltitle)}`;
                }
            }

            const components = row ? [row] : [];

            const allowedMentions = { repliedUser: false };

            if (info.filesize >= UPLOAD_LIMIT) {
                const upload_command = config.upload_command.replace('{path}', uploadPath);
                const response = await exec(upload_command);

                const url = new URL(response.stdout);

                await msg.reply({ content: content !== undefined ? (content + ` (${url.href})`) : json.url, embeds, allowedMentions, components });
            } else {
                if (embeds.length > 0) {
                    await msg.reply({
                        content,
                        files: [{
                            attachment: uploadPath,
                            name: fileName,

                        }],
                        allowedMentions
                    });
                } else {
                    await msg.reply({
                        content,
                        files: [{
                            attachment: uploadPath,
                            name: fileName,

                        }],
                        components,
                        allowedMentions
                    });
                }
            }

            if (sitesDelEmbed.some(a => arg.includes(a))) {
                msg.suppressEmbeds(true).catch(console.error);
            }

            msg.reactions.cache.find(a => a.me).remove().catch(console.error);
        }
    }
};
