const config = require('./config.json');
const helper = require('./helper.js');
const subForums = require('./subForums.json');
const mayorData = require('./mayorData.json');
const reminderModule = require('./commands/rem.js');
const axios = require('axios');
const WebSocket = require('ws');
const stripAnsi = require('strip-ansi');
const anilist = require('anilist-node');
const Anilist = new anilist();
const voteMuteCommand = require('./votemute');

const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');
const Discord = require('discord.js');
const { 
	ActionRowBuilder, 
	SelectMenuBuilder, 
    ModalBuilder,
	ButtonBuilder, 
	ButtonStyle, 
    TextInputBuilder,
    TextInputStyle,
	PermissionsBitField, 
	GatewayIntentBits, 
	Partials, 
	inlineCode 
} = Discord;

let lodestone;

const emoji = require('node-emoji');
const fetch = require('node-fetch');
const EventEmitter = require('events');
const fs = require('fs').promises;
const path = require('path');
const objectPath = require("object-path");
const chalk = require('chalk');
const _ = require('lodash');
const Keyv = require('keyv');
const Parser = require('rss-parser');
const parser = new Parser();
const tesseract = require("node-tesseract-ocr");

const MAYORS = {
	Cole: {
		texture: '16422de08848952d1cbead66bbbad6f07191bdcc952f3d1036aeb0c22938f39b'
	},
	Diaz: {
		texture: '9cf4737cd444b590545734a6408cbe23c182f4283f167a3e3c09532ccbef17f9'
	},
	Foxy: {
		texture: '3485a717fa0f51d7fadc66a5d5e9853905bef914e3b2848a2f128e63d2db87'
	},
	Paul: {
		texture: '1b59c43d8dbccfd7ec6e6394b6304b70d4ed315add0494ee77c733f41818c73a'
	},
	Barry: {
		texture: 'f04c591b164746e848f3d6a451ee87a62dd193e5c45e94ed78e72df119aca426'
	},
	Derpy: {
		texture: 'f450d12692886c47b078a38f4d492acfe61216e2d0237ab82433409b3046b464'
	},
	Diana: {
		texture: '83cc1cf672a4b2540be346ead79ac2d9ed19d95b6075bf95be0b6d0da61377be'
	},
	Aatrox: {
		texture: 'c1bdf505bb8c0f1f3365a03032de1931663ff71c57e022558de312b8f1b5c445'
	},
	Marina: {
		texture: '807fc9bee8d3344e840e4031a37249a4c3c87fc80cf16432cc5c2153d1f9c53d'
	},
	Scorpius: {
		texture: '8f26fa0c47536e78e337257d898af8b1ebc87c0894503375234035ff2c7ef8f0'
	},
	Dante: {
		texture: '5af658e00ac0d0ce0686e79f59c067b9577c01ba57ad8c6575db8490c3161772'
	},
	Finnegan: {
		texture: 'e7747fbee9fb39be39b00d3d483eb2f88b4bae82417ab5cb1b1aa930dd7b6689'
	},
	Jerry: {
		texture: 'https://static.wikia.nocookie.net/hypixel-skyblock/images/5/58/Villager.png'
	}
}

const COLOR_ROLES = [
	{
		name: 'Coral',
		role: '739935380614152253',
		emote: '739936842350198904',
	}, {
		name: 'Cantaloupe',
		role: '739935842428256297',
		emote: '739936841670852739',
	}, {
		name: 'Mint',
		role: '739934953487466591',
		emote: '739936842010329098',
	}, {
		name: 'Rose',
		role: '739934023408943135',
		emote: '739936841976774766',
	}, {
		name: 'Salmon',
		role: '739934652680372264',
		emote: '739936841842819113',
	}, {
		name: 'Maya',
		role: '739935198921359370',
		emote: '739943751308410951',
	}, {
		name: 'Lavender',
		role: '740114379088855131',
		emote: '740115305006891028',
	}, {
		name: 'Mikado',
		role: '841407225490964492',
		emote: '841408338238898196',
	}, {
		name: 'Cream',
		role: '900507916497473576',
		emote: '900646057917177856'
	}, {
		name: 'Peach',
		role: '940740059504738334',
		emote: '940739930404053023'
	}
];

const UNRECLAIMABLE_ROLES = [
	"683487784820015181", // Member
	"708524526958280734", // Server Boosters
	"683981275270480038", // Patrons
	"682878669198458926", // Mods
	"684049569784660108" // Muted
];

UNRECLAIMABLE_ROLES.push(...COLOR_ROLES.map(x => x.role));

const ROLE_ICONS = ["🐒", "🐔", "🐧", "🐣", "🦉", "🦄", "🐝", "🐛", "🦋", "🐌", "🐢", "🐍", "🦝", "🐟", "🐳", "🦧", "🐘", "🐈", "🐀", "🦦", "🦥", "🐕", "🦆", "🦍"];

const rest = new REST({ version: '9' }).setToken(config.credentials.bot_token);

const endEmitter = new EventEmitter();

const db = new Keyv(config.dbUri, { namespace: config.dbNamespace });

const client = new Discord.Client({ 
	intents: Object.values(GatewayIntentBits), 
	partials: Object.values(Partials) 
});

async function checkUpdates() {
	for (const forumId in subForums) {
		const forum = subForums[forumId];

		const feed = await parser.parseURL(`https://hypixel.net/forums/${forumId}.rss`);

		for (const item of feed.items) {
			if (Array.isArray(forum.authors) && forum.authors.includes(item.creator) === false) {
				continue;
			}

			if (Array.isArray(forum.has) && forum.has.every(a => item.title.toLowerCase().includes(a.toLowerCase())) === false) {
				continue;
			}

			const guid = parseInt(item.guid);

			if (guid <= forum.latest) {
				continue;
			}

			if (Object.values(subForums).map(a => a.latest).includes(guid)) {
				continue;
			}

			const channel = client.channels.cache.get("710371355970895964");
			const skyblockChannel = client.channels.cache.get("798149664012501032");

			const announcementMsg = await channel.send(`https://hypixel.net/threads/${guid} <@&1063547431033249902>`);
			announcementMsg.crosspost().catch(console.error);

			await skyblockChannel.send(`hewwo (・\`ω´・) yuw might find dis intewesting: https://hypixel.net/threads/${guid}`);

			forum.latest = guid;
			await fs.writeFile('./subForums.json', JSON.stringify(subForums));
		}
	}
}

let jacobsContests = [];

async function updateContests() {
	const response = await fetch('https://dawjaw.net/jacobs');
	const json = await response.json();

	jacobsContests = json;
}

const getRawLore = text => {
	let output = "";
	let parts = text.split("§");

	for (const [index, part] of parts.entries())
		output += part.substring(Math.min(index, 1));

	return output;
};

let lastTotalVotes = 0;
const MAYOR_CHANNEL = '710371355970895964';
let currentElectionMessage;

async function sendReminders() {
	const reminders = await db.get('reminders') ?? [];
	const sendReminders = reminders.filter(r => Date.now() >= r.at);
	await db.set('reminders', reminders.filter(r => r.at > Date.now()));

	for (const reminder of sendReminders) {
		let channel;

		if (reminder.guild) {
			const guild = await client.guilds.fetch(reminder.guild);
			channel = await guild.channels.fetch(reminder.in);
		} else {
			channel = await (await client.users.fetch(reminder.author)).createDM();
		}

		let remText = `<@${reminder.author}>`;

		for (const user of reminder.cc) {
			remText += ` <@${user}>`;
		}

		remText += ': ';
		remText += reminder.with ? reminder.with : 'i hope u wember (≧◡≦)';

		try {
			await channel.send(remText);

			const msg = await channel.messages.fetch(reminder.msg);

			const embed = Object.assign({}, msg?.embeds?.[0]?.data ?? {});

			if (embed?.description == null) {
				return;
			}

			embed.description = embed.description.replace('Weminding', 'Weminded');

			await msg.edit({ embeds: [embed], components: [] });
		} catch(e) {
			helper.error('Failed sending reminder');
			helper.error(e);
		}
	}
}

async function checkElection() {
	let { currentElectionMessageId, electionYear } = mayorData;

	const response = await fetch('https://api.hypixel.net/resources/skyblock/election');
	const json = await response.json();

	const election = json.current ?? json.mayor.election;

	const electionMessage = { content: `Mayor Candidates for SkyBlock Year ${election.year}`, embeds: [] };

	const timestamp = 1560275700000 + 446400000 * election.year + 105600000;
	const ended = Date.now() > timestamp;

	const candidates = election.candidates.sort((a, b) => b.votes - a.votes);
	const totalVotes = candidates.map(a => a.votes).reduce((b, a) => b + a, 0);

	if (totalVotes == lastTotalVotes) {
		return;
	}

	lastTotalVotes = totalVotes;

	let footerText = 'Active Until:';

	if (!isNaN(totalVotes)) {
		footerText = footerText + `Total Votes: ${totalVotes.toLocaleString()} • `;
	}

	const footer = {
		text: footerText
	};

	const embed = { fields: [], footer, timestamp };

	for (const [index, candidate] of candidates.entries()) {
		const winner = index == 0 && Date.now() > timestamp;
		const minister = index == 1 && Date.now() > timestamp;

		const field = {
			name: `${candidate.name}`,
			value: '',
			inline: false
		};

		if (!isNaN(candidate.votes)) {
			field.name += ` (${Math.round((candidate.votes / totalVotes) * 100)}%)`;
		}

		if (winner) {
			electionMessage.content = `${candidate.name} won the Mayor Election for SkyBlock Year ${election.year}!`;

			let mayorImg = MAYORS[candidate.name]?.texture ?? null;

			if (!mayorImg.startsWith('http')) {
				mayorImg = `https://mc-heads.net/body/${mayorImg}/left`;
			}

			embed.thumbnail = {
				url: mayorImg
			};

			embed.footer = { text: `\nTotal Votes: ${totalVotes.toLocaleString()} • Next Election Starts:` };
			embed.timestamp += 111600000;

			field.name += ' • Winner';
		}

		if (minister) {
			field.name += ' • Minister';
		}

		for (const [index, perk] of candidate.perks.entries()) {
			if (index > 0) {
				field.value += '\n';
			}

			let perkDescription = getRawLore(perk.description);

			if (minister && perk.minister) {
				perkDescription = `**${perkDescription}**`;
			}

			if (perk.minister) {
				perkDescription = `⭐ ${perkDescription}`;
			} else {
				perkDescription = `▫️ ${perkDescription}`;
			}

			if (winner) {
				perkDescription = `**${perkDescription}**`;
			}

			field.value += perkDescription;
		};

		embed.fields.push(field);
	};

	embed.fields.push({
		name: '‎ ',
		value: '‎ ',
		inline: true
	});

	embed.timestamp = new Date(embed.timestamp).toISOString();

	electionMessage.embeds.push(embed);

	const channel = client.channels.cache.get(MAYOR_CHANNEL);

	if (election.year > electionYear) {
		const _currentElectionMessage = await channel.send(electionMessage);

		mayorData.electionYear = election.year;
		mayorData.currentElectionMessageId = _currentElectionMessage.id;

		currentElectionMessage = _currentElectionMessage;

		await fs.writeFile('./mayorData.json', JSON.stringify(mayorData));
	} else if (currentElectionMessageId != null || currentElectionMessage != null) {
		if (currentElectionMessage == null) {
			currentElectionMessage = await channel.messages.fetch(currentElectionMessageId);
		}

		await currentElectionMessage.edit(electionMessage);
		console.log('edited msg');
	}
}

function updateStatus() {
	client.user.setActivity(`Femboy Fishing Ep ${Math.floor(Math.random() * 9) + 1}`, { type: 'WATCHING' });
}

function updateChecks() {
	checkUpdates().catch(helper.error);
	checkElection().catch(helper.error);
}

function updateReminders() {
	sendReminders().catch(helper.error);
}

client.on('error', helper.error);
client.on('ready', () => {
	updateStatus();
	updateChecks();
	updateReminders();
	updateContests();

	setInterval(updateChecks, 60 * 1000);
	setInterval(updateStatus, 60 * 60 * 1000);
	setInterval(updateReminders, 1000);
	setInterval(updateContests, 20 * 60 * 1000);
});

const commands = [];
const commandsPath = path.resolve(__dirname, 'commands');

const handlers = [];
const handlersPath = path.resolve(__dirname, 'handlers');

async function reloadCommands() {
	while (commands.length)
		commands.pop();

	while (handlers.length)
		handlers.pop();

	for (const key of Object.keys(require.cache))
		if (key.includes('/commands/') || key.includes('/handlers/'))
			delete require.cache[key];

	try {
		const items = await fs.readdir(commandsPath);

		for (const item of items) {
			if (path.extname(item) == '.js') {
				const command = require(path.resolve(commandsPath, item));

				command.filename = path.resolve(commandsPath, item);

				let available = true;
				const unavailabilityReason = [];

				if (command.folderRequired !== undefined && command.folderRequired.length > 0) {
					const folderRequired = _.castArray(command.folderRequired);

					for (const folder of folderRequired) {
						if (!fs.existsSync(path.resolve(__dirname, folder)))
							available = false;
						unavailabilityReason.push(`required folder ${folder} does not exist`);
					}
				}

				if (command.configRequired !== undefined && command.configRequired.length > 0) {
					const configRequired = _.castArray(command.configRequired);

					for (const configPath of configRequired) {
						if (!objectPath.has(config, configPath)) {
							available = false;
							unavailabilityReason.push(`required config option ${configPath} not set`);
						} else if (objectPath.get(config, configPath).length == 0) {
							available = false;
							unavailabilityReason.push(`required config option ${configPath} is empty`);
						}
					}
				}

				if (command.emoteRequired !== undefined && command.emoteRequired.length > 0) {
					const emoteRequired = _.castArray(command.emoteRequired);

					for (const emoteName of emoteRequired) {
						const emote = helper.emote(emoteName, null, client);

						if (!emote) {
							available = false;
							unavailabilityReason.push(`required emote ${emoteName} is missing`);
						}
					}
				}

				if (available) {
					commands.push(command);

					if (config.debug)
						console.log(chalk.green(`${config.prefix}${command.command[0]} successfully enabled.`));
				} else {
					command.command = _.castArray(command.command);

					console.log('');
					console.log(chalk.yellow(`${config.prefix}${command.command[0]} was not enabled:`));

					for (const reason of unavailabilityReason)
						console.log(chalk.yellow(reason));
				}
			}
		}
	} catch (e) {
		console.error("Unable to read commands folder");
		throw e;
	}

	try {
		const items = await fs.readdir(handlersPath);

		for (const item of items) {
			if (path.extname(item) == '.js') {
				const handler = require(path.resolve(handlersPath, item));
				handlers.push(handler);
			}
		}
	} catch (e) {
		console.error("Unable to read handlers folder");
		throw e;
	}

	helper.init(commands, db);
}

async function onMessage(msg, isEdit = false) {
	if (msg.author.id == client.user.id)
		return;

	const prefix = await helper.prefix(msg.guild);
	const extendedLayout = await helper.extendedLayout(msg);

	// removes escape backslashes from bridged messages
	// not perfect but will at least fix most args with special characters
	if (msg.webhookId != null)
		msg.content = msg.content.replace(/\\(?!\\)/g, '');

	const argv = msg.content.split(' ');

	const guildId = msg?.guild?.id;

	argv[0] = argv[0].substr(prefix.length);

	if (config.debug)
		helper.log(isEdit ? '(edit)' : '', msg.author.username, ':', msg.content);

	if (!isEdit) {
		for (const handler of handlers) {
			if (handler.message && typeof handler.message === 'function') {
				handler.message({
					msg,
					argv,
					client,
					prefix,
					extendedLayout,
					db,
					lodestone
				}).catch(helper.error);
			}
		}
	}

	if (msg.author.bot && msg.webhookId == null)
		return;

	let responseMsg;

	if (isEdit) {
		for (const embed of msg.embeds) {
			if (embed.type == 'image') {
				checkWysi(embed.url, msg);
			}
		}

		const responseMsgId = await db.get(`response_${guildId}_${msg.channel.id}_${msg.id}`);

		if (responseMsgId == null)
			return;

		const split = responseMsgId.split("_");
		responseMsg = msg.channel.messages.cache.get(split[2]);

		if (responseMsg == null)
			return;
	}

	if (!isEdit) {
		msg.attachments.forEach(file => {
			if (['image/jpeg', 'image/png'].includes(file.contentType)) {
				checkWysi(file.url, msg);
			}
		});
	}

	if (!isEdit && msg.channel.id == '909298825758400545') {
		const msgContent = msg.content.toLowerCase();
		const msgParts = msgContent.split(' ');

		if (msg.author.id == '372767865096372227' 
		&& msgContent.startsWith('fishe fact time!')) {
			msg.reply(`<@&1012640333563166811> guys new fishe fact just dwopped! feel fwee to ask me to subswibe yuw (≧◡≦)`).catch(console.error);
		}

		if (
		    ['fish', 'fishe'].some(a => msgParts.includes(a))
		 && ['fact', 'facts'].some(a => msgParts.includes(a))
		) {
			if (
				['subscwibe', 'subscribe', 'sub'].some(a => msgParts.includes(a))
			) {
				if (!msg.member.roles.cache.has('1012640333563166811')) {
					await msg.member.roles.add('1012640333563166811').catch(console.error);
					msg.reply('you awe now subscwibed to daily fishe fact 🐟 <:starry:855446674793168956>').catch(console.error);
				} else {
					msg.reply('you awe alweady subscwibed to daily fishe fact <:Think:790399525495898122>').catch(console.error);
				}
			} else if (
				['unsubscwibe', 'unsubscribe', 'unsub'].some(a => msgParts.includes(a))
			) {
				if (msg.member.roles.cache.has('1012640333563166811')) {
					await msg.member.roles.remove('1012640333563166811').catch(console.error);
					msg.reply('you awe now unsubscwibed from daily fishe fact (>﹏<)').catch(console.error);
				} else {
					msg.reply('you awe not subscribed to daily fishe fact <:Think:790399525495898122>').catch(console.error);
				}
			}
		}
	}

	if (msg.member != null
		&& msg.member.permissions.has(PermissionsBitField.Flags.Administrator)
		&& msg.content.startsWith('!robotleaprefix')) {
		const newPrefix = msg.content.substring('!robotleaprefix'.length).trim();

		try {
			await db.set(`pfx_${msg.guild.id}`, newPrefix);
			await msg.channel.send(`Prefix updated to \`${newPrefix}\``);

			return;
		} catch (e) {
			helper.error(e);
		}
	}

	if (_.castArray(config.owners).includes(msg.author.id)
		&& msg.content.startsWith('!reloadcommands'))
		client.shard.send('reloadCommands').catch(() => { });

	for (const command of commands) {
		if (msg.guild != null && msg.guild.id == '339261635191504896' && !['np', 'rem', 'jw', 'uwuify', 'ptw'].includes(command.command[0]))
			continue;

		const commandMatch = await helper.checkCommand(prefix, msg, command);

		if (commandMatch === true && typeof command.call === 'function') {
			if (isEdit)
				endEmitter.emit(`end-${guildId}_${responseMsg.channel.id}_${responseMsg.id}`);

			let response, messagePromise;

			try {
				response = await command.call({
					guildId,
					msg,
					argv,
					client,
					prefix,
					extendedLayout,
					responseMsg,
					endEmitter,
					db,
					jacobsContests
				});
			} catch (e) {
				helper.error(e);

				if (typeof e === 'object' && e.embed != null)
					response = e;
				else
					response = {
						embeds: [{
							color: helper.errorColor,
							author: {
								name: 'Error'
							},
							description: e.toString()
						}]
					};
			}

			if (response instanceof Discord.Message) {
				await db.set(
					`response_${guildId}_${msg.channel.id}_${msg.id}`,
					`${guildId}_${response.channel.id}_${response.id}`,
					2 * 60 * 1000);

				return;
			}

			if (typeof response === 'string')
				response = { embeds: [{ color: helper.mainColor, description: response }] };

			if (response == null)
				return;

			if (isEdit)
				messagePromise = responseMsg.edit(response);
			else
				messagePromise = msg.channel.send(response);

			messagePromise.then(async message => {
				await db.set(
					`response_${guildId}_${msg.channel.id}_${msg.id}`,
					`${guildId}_${message.channel.id}_${message.id}`,
					2 * 60 * 1000);
			});

			try {
				await messagePromise;
			} catch (e) {
				message = await msg.channel.send({
					embeds: [{
						color: helper.errorColor,
						author: {
							name: 'Error'
						},
						description: e
					}]
				});
			}
		} else if (commandMatch !== false) {
			let msgObj;

			if (typeof commandMatch == 'string')
				msgObj = { embeds: [{ color: helper.mainColor, description: commandMatch }] };
			else
				msgObj = commandMatch;

			const message = await msg.channel.send(msgObj);

			await db.set(
				`response_${guildId}_${msg.channel.id}_${msg.id}`,
				`${guildId}_${message.channel.id}_${message.id}`,
				2 * 60 * 1000);
		}
	}
}

const tesseractConfig = {
	lang: "eng", // default
	oem: 3,
	psm: 3,
	tessedit_char_whitelist: "0123456789",
}

async function checkWysi(url, message) {
    if (!config.enableWysi) return;
    
	const text = await tesseract.recognize(url, tesseractConfig);

	console.log(text);

	if (text.includes('727')) {
		message.reply('<a:WYSI:802721803755061301>').catch(console.error);
	}
}

let colorMsgObj;

const getReclaimableRoles = async member => {
	let reclaimable = await db.get(`reclaimroles_${member.id}`) ?? [];

	reclaimable = reclaimable.filter(r => 
	   !UNRECLAIMABLE_ROLES.includes(r)
	&& !member.roles.cache.has(r)
	);

	return [...new Set(reclaimable)];
}

async function onInteraction(interaction) {
    if (interaction.commandName === 'votemute') {
        voteMuteCommand.call({ interaction, db, client }).catch(console.error);
        return;
    }

	if (interaction.commandName === 'icon') {
		const options = [{ label: 'None', value: 'none' }];

		for (const icon of ROLE_ICONS) {
			const e = emoji.find(icon);

			options.push({ label: `${icon} ${_.capitalize(e.key.replace('_', ' '))}`, value: icon });
		}

		const row = new ActionRowBuilder()
			.addComponents(
				new SelectMenuBuilder()
					.setCustomId('roleIcon')
					.setPlaceholder('Select role icon')
					.addOptions(options),
			);

		await interaction.reply({ content: 'Select your new role icon!', ephemeral: true, components: [row] });
		return;
	}

	if (interaction.commandName === 'reclaimroles') {
		const reclaimable = await getReclaimableRoles(interaction.member);

		const embed = {
			color: helper.mainColor
		};

		if (reclaimable.length == 0) {
			embed.description = 'You have no roles to reclaim!';

			await interaction.reply({ embeds: [embed], ephemeral: true });
			return;
		}

		const roles = await interaction.guild.roles.fetch();

		const options = [{
			label: 'All Roles',
			value: 'all'
		}];

		embed.description = `You have ${inlineCode(reclaimable.length)} role${reclaimable.length > 1 ? 's' : ''} to reclaim:`;

		for (const [index, roleId] of reclaimable.entries()) {
			const role = roles.get(roleId);
			options.push({ label: role.name, value: roleId });

			if (index > 0) {
				embed.description += ',';
			}
			embed.description += ` <@&${roleId}>`;
		}

		const row = new ActionRowBuilder()
		.addComponents(
			new SelectMenuBuilder()
				.setCustomId('reclaimroles-select')
				.setPlaceholder('Select roles to reclaim')
				.addOptions(options),
		);

		await interaction.reply({ embeds: [embed], components: [row], ephemeral: true });
	
		return;
	}

    if (interaction.customId == 'link-anilist-account') {
        const modal = new ModalBuilder()
        .setCustomId('link-anilist-modal')
        .setTitle('Link Anilist Account');

        const anilistUserInput = new TextInputBuilder()
        .setCustomId('anilist-user-input')
        .setLabel('Username or User ID')
        .setStyle(TextInputStyle.Short);

        const row = new ActionRowBuilder().addComponents(anilistUserInput);
        modal.addComponents(row);

        await interaction.showModal(modal);

        return;
    }

    if (interaction.customId == 'link-anilist-modal') {
        interaction.deferReply({ ephemeral: true }).catch(helper.error);

        try {
            const username = interaction.fields.getTextInputValue('anilist-user-input');
            const user = await Anilist.user.profile(username);

            await db.set(`anilist_user_${interaction.user.id}`, user.id);

            await interaction.followUp('Account successuwwy winked!');
        } catch(e) {
            await interaction.followUp('Faiwed to wink account, wrong username maybe?');
        }

        return;
    }

	if (interaction.customId?.startsWith('get-images-')) {
		const id = interaction.customId.split('-').pop();
		const imagesPath = `./downloads/${id}`;

		try {
			await fs.access(imagesPath);

			const images = await fs.readdir(imagesPath);

			for(let i = 0; i < Math.ceil(images.length / 9); i++) {
				const files = images.slice(i * 9, (i + 1) * 9).map(a => `${imagesPath}/${a}`);

				if (i == 0) {
					await interaction.reply({
						ephemeral: true,
						files
					});
				} else {
					await interaction.followUp({
						ephemeral: true,
						files
					});
				}
			}

			return;
		} catch(e) {
			console.error(e);
			return;
		}
	}

	if (interaction.customId == 'reclaimroles-select') {
		const [roleId] = interaction.values;
		const reclaimable = await getReclaimableRoles(interaction.member);

		let roles = new Array();
		
		if (roleId == 'all') {
			roles.push(...reclaimable);
		} else {
			roles.push(roleId);
		}

		roles = roles.filter(r => reclaimable.includes(r));

		if (roles.length == 0) {
			await interaction.reply({
				content: 'No more roles to reclaim!',
				ephemeral: true
			});
		}

		await interaction.member.roles.add(roles);

		await interaction.reply({
			content: `Successfully reclaimed <@&${roles.join('> <@&')}>`,
			ephemeral: true
		});

		return;
	}

	if (interaction.commandName === 'selfmute') {
		const hours = interaction.options.get('hours')?.value;

		const description
		= `You will be muted until <t:${Math.floor((Date.now() + hours * 3600 * 1000) / 1000)}>. `
		+ 'This will not be lifted by a staff member. To proceed click one of '
		+ 'the two buttons: One will send a message in chat, the other will mute you silently.';

		const row = 
		new ActionRowBuilder()
		.addComponents(
			new ButtonBuilder()
				.setCustomId(`selfmute-${hours}-broadcast`)
				.setLabel('Mute')
				.setStyle(ButtonStyle.Danger),
			new ButtonBuilder()
				.setCustomId(`selfmute-${hours}-silent`)
				.setLabel('Mute silently')
				.setStyle(ButtonStyle.Secondary)
		);

		const embed = {
			color: helper.mainColor,
			description
		};

		await interaction.reply({
			embeds: [embed],
			components: [row],
			ephemeral: true
		});

		return;
	}

	if (interaction.customId === 'set-contest-reminder') {
		const { id } = interaction.user;
		const value = interaction.values[0];
		const time = Number(value.split('-').pop());
		const contest = jacobsContests.find(a => a.time == time);

		if (contest == null || time < Date.now / 1000) {
			return;
		}

		const reminders = await db.get('reminders') ?? [];

		const at = (time - 5 * 60) * 1000;

		if (Date.now() > at) {
			await interaction.reply({
				content: `This contest is in wess than 5 minutes, why wouwd u need a weminder? ?_?`,
				ephemeral: true
			});
			return;
		}

		if (reminders.find(a => 
			   a.at == at 
			&& a.author == id
			&& a.in == interaction?.channel?.id)) {
			
		}

		const contestReminder = reminders.find(a => 
			a.at == at 
		 && a.in == interaction?.channel?.id);

		 if (contestReminder && (contestReminder.author == id || contestReminder.cc.includes(id))) {
			await interaction.reply({
				content: `I'm alweady weminding u for this contest. (≧◡≦)`,
				ephemeral: true
			});

			return;
		 }

		 if (contestReminder) {
			if (contestReminder.author != id && !contestReminder.cc.includes(id)) {
				contestReminder.cc.push(id);
			}
		} else {
			reminders.push({
				id: reminders.length > 0 ? reminders.at(-1).id + 1 : 0,
				msg: null,
				cc: [],
				guild: interaction.guildId,
				author: interaction.user.id,
				at,
				in: interaction?.channel?.id ?? interaction.user.id,
				with: `${contest.crops.join(' / ')} Contest <t:${at / 1000}:R>!`
			});
		}

		await db.set('reminders', reminders);
		await interaction.reply({
			content: `Weminding you 5 minutes before the ${contest.crops.join(' / ')} Contest <t:${time}:R>. („• ᴗ •„)`,
			ephemeral: true
		});

		return;
	}

	if (interaction.customId?.startsWith('selfmute-')) {
		const [,hours,type] = interaction.customId.split('-');

		const timeoutMs = Number(hours) * 1000 * 3600;
		const muteUntil = Math.floor((Date.now() + timeoutMs) / 1000);
		await interaction.member.timeout(timeoutMs, 'Self-mute');

		if (type == 'silent') {
			await interaction.reply({
				content: `You have successfully been muted until <t:${muteUntil}:R>.`,
				ephemeral: true
			});
		} else {
			await interaction.reply({
				content: `<@${interaction.member.id}> has muted themselves. They will be unmuted <t:${muteUntil}:R>.`
			});
		}
	}

	if (interaction.customId?.startsWith('remindme-')) {
		const reminderId = Number(interaction.customId.replace('remindme-', ''));

		if (isNaN(reminderId)) {
			return;
		}

		const reminders = await db.get('reminders');
		const reminder = reminders.find(r => r.id == reminderId);

		if (reminder == null) {
			return;
		}

		const { id } = interaction.user;

		if (reminder.cc.includes(id)) {
			reminder.cc = reminder.cc.filter(m => m != id);
		} else if (reminder.author != id) {
			reminder.cc.push(id);
		}

		await db.set('reminders', reminders);

		const { message } = interaction;
		const { components } = message;

		if (components.length == 0) {
			return;
		}

		let label = 'Remind me too!';

		if (reminder.cc.length > 0) {
			label += ` (${reminder.cc.length} ${reminder.cc.length > 1 ? 'people' : 'person'})`;
		}

		const row = 
		new ActionRowBuilder()
		.addComponents(
			new ButtonBuilder()
				.setCustomId(`remindme-${reminder.id}`)
				.setLabel(label)
				.setEmoji('🔔')
				.setStyle(ButtonStyle.Primary),
		);

		await interaction.update({ embeds: message.embeds, components: [row] });

		return;
	}

	if (interaction.customId === 'delete-reminder') {
		const value = interaction.values[0];
		const reminderId = Number(value);

		let reminders = await db.get('reminders');
		const reminder = reminders.find(r => r.id == reminderId);

		if (reminder == null) {
			await interaction.followUp({
				ephemeral: true,
				content: 'Weminder not found, either it alweady happened or it was alweady deleded. (>_<)'
			});

			return;
		}

		if (reminder.author != interaction.user.id) {
			return;
		}

		reminders = reminders.filter(r => r.id != reminderId);

		await db.set('reminders', reminders);
		await interaction.update(reminderModule.getList(interaction.user.id, interaction?.guild?.id, reminders));

		return;
	}

	const skyleaGuild = await client.guilds.fetch('680401335862296611');
	const privateChat = await skyleaGuild.channels.fetch('756423244013109248');

	if (interaction.customId === 'join-private-chat') {
		await privateChat.permissionOverwrites.edit(interaction.member, { ViewChannel: true });
		await interaction.reply({ content: `You have successfully joined <#${privateChat.id}>.`, ephemeral: true });
	}

	if (interaction.customId === 'leave-private-chat') {
		await privateChat.permissionOverwrites.edit(interaction.member, { ViewChannel: false });
		await interaction.reply({ content: `You have successfully left <#${privateChat.id}>.`, ephemeral: true });
	}

	if (!interaction.isStringSelectMenu())
		return;

	if (interaction.customId === 'roleIcon') {
		const value = interaction.values[0];

		if (!ROLE_ICONS.includes(value))
			return;
	} else if (interaction.customId === 'colorRole') {
		const value = interaction.values[0];

		interaction.member.roles.add('842549363153829928').catch(console.error);

		if (value == 'none') {
			await interaction.member.roles.remove(COLOR_ROLES.map(a => a.role));
			await interaction.update(colorMsgObj);
			return;
		}

		const colorRole = COLOR_ROLES.find(a => a.name == value);

		if (colorRole == null)
			return;

		const removedColors = COLOR_ROLES.filter(a => a.role != colorRole.role);

		await interaction.member.roles.remove(removedColors.map(a => a.role));
		await interaction.member.roles.add(colorRole.role);

		await interaction.update(colorMsgObj);
	}
}

async function onLodestoneEvent(msg) {
	const eventObj = JSON.parse(msg);

	if (eventObj.event_inner == null) {
		return;
	}

	if (eventObj.event_inner.instance_event_inner == null) {
		return;
	}

	const { instance_event_inner: event } = eventObj.event_inner;

	if (event.type == 'SystemMessage' && event.message.split(' ')?.[3] == '[Not') {
		event.type = 'PlayerMessage';
		event.player = event.message.split('<')[1].split('>')[0];
		event.player_message = event.message.split('>').slice(1).join('>').trim();
	}

	if (event.type == 'PlayerMessage') {
		try {
			const lastMsg = lodestone.bridgeChannel.lastMessage;
			const [embed] = lastMsg.embeds;

			if (embed.description.startsWith(`**${event.player}**`)) {
				const _embed = {
					description: embed.description + `\n**${event.player}**: ${event.player_message}`
				};

				await lastMsg.edit({
					embeds: [_embed]
				});

				return;
			}
		} catch(e) {

		}

		lodestone.bridgeChannel.send({ embeds: [{ description: `**${event.player}**: ${event.player_message}`}]}).catch(helper.error);
	} else if (event.type == 'SystemMessage') {
		const systemMsg = stripAnsi(event.message).trim().split(':').pop();

		if (systemMsg.endsWith('the game')) {
			lodestone.bridgeChannel.send({
				embeds: [
					{
						description: systemMsg,
						color: systemMsg.includes(' joined') ? 0x2cc92e : 0xc92c2c,
					}
				]
			}).catch(helper.error);
		}
	}
}

async function updatePlayerCount() {
	const response = await axios.get(`${lodestone.host}/api/v1/instance/list`, {
		headers: {
			Authorization: `Bearer ${lodestone.token}`
		}
	});

	if (response.data.length < 1) {
		return;
	}

	const { player_list, player_count } = response.data[0];

	let topic = `${player_count} players online – info on how to join: <#809571758738767922> – map: https://map.lea.moe`;

	if (player_count > 0) {
		topic += ` – \nplayers: ${player_list.map(a => stripAnsi(a.name)).join(', ')}`
	}

	await lodestone.bridgeChannel.edit({ topic });		
}

async function main() {
	await reloadCommands();

	if (config.lodestone?.bridgeChannelId) {
		lodestone = config.lodestone;
		const auth = Buffer.from(`${lodestone.username}:${lodestone.password}`).toString('base64');

		const response = await axios.post(`${lodestone.host}/api/v1/user/login`, {}, {
			headers: {
				Authorization: `Basic ${auth}`
			}
		});

		const wsHost = lodestone.host.replace('http', 'ws');

		lodestone.token = response.data.token;
		const ws = new WebSocket(`${wsHost}/api/v1/instance/${lodestone.instanceId}/console/stream?token=Bearer ${lodestone.token}`);
		ws.on('message', onLodestoneEvent);
	}

	client.on('interactionCreate', onInteraction);
	client.on('messageCreate', onMessage);
	client.on('messageUpdate', (oldMsg, newMsg) => { onMessage(newMsg, true); });
	client.on('messageDelete', async msg => {
		const guildId = msg.guild != null ? msg.guild.id : 'me';

		if (config.debug)
			helper.log('(delete)', msg.author.username, ':', msg.content);

		const responseMsgId = await db.get(`response_${guildId}_${msg.channel.id}_${msg.id}`);

		if (responseMsgId == null)
			return;

		const split = responseMsgId.split("_");
		const responseMsg = msg.channel.messages.cache.get(split[2]);

		if (responseMsg == null)
			return;

		endEmitter.emit(`end-${guildId}_${responseMsg.channel.id}_${responseMsg.id}`);
		responseMsg.delete().catch(console.error);
	});

	try {
		await client.login(config.credentials.bot_token);
	} catch (e) {
		console.error('');
		console.error(chalk.redBright("Couldn't log into Discord. Wrong bot token?"));
		console.error('');
		console.error(e);
		process.exit();
	}

	client.on('ready', async () => {
		if (lodestone?.bridgeChannelId) {
			lodestone.bridgeChannel = await client.channels.fetch(lodestone.bridgeChannelId);
			updatePlayerCount().catch(helper.error);
			setInterval(updatePlayerCount, 1000 * 300);
		}

		await rest.put(
			Routes.applicationGuildCommands(config.credentials.discord_client_id, config.mainGuild),
			{
				body: [{
					name: 'icon',
					description: "Allows you to choose from a list of role icons.",
					options: []
				}, {
					name: 'reclaimroles',
					description: "Reclaim old roles after rejoining server.",
					options: []
				}, {
					name: 'selfmute',
					description: "Mute yourself for a specified amount of hours (max. 48 hours).",
					options: [{
						required: true,
						type: 10,
						name: 'hours',
						min_value: 0.5,
						max_value: 48,
						description: 'Amount of hours to mute yourself for'
					}],
				}, {
                    name: 'votemute',
                    description: "Vote mute a member.",
                    options: [
                        {
                            name: 'member',
                            description: 'Member to mute',
                            type: 6,
                            required: true
                        },
                        {
                            name: 'reason',
                            description: 'Reason',
                            type: 3,
                            required: true
                        }
                    ]
                }]
			},
		).catch(console.error);

		const guild = client.guilds.cache.get('680401335862296611');
		const colorChannel = guild.channels.cache.get('739933850813071361');
        const heartChannel = guild.channels.cache.get('720005615040069662');
		const colorMsg = await colorChannel.messages.fetch('739940971361599519');

		const filter = (reaction, user) => user.id != client.user.id;

		const collector = colorMsg.createReactionCollector({ filter, dispose: true });

		collector.on('collect', async (reaction, user) => {
			const guildMember = await guild.members.fetch(user.id);
			const colorRole = COLOR_ROLES.find(a => a.emote == reaction.emoji.id);
			const removedColors = COLOR_ROLES.filter(a => a.role != colorRole.role);

			// await guildMember.roles.remove(removedColors.map(a => a.role));

			await guildMember.roles.add(colorRole.role)
				.catch(console.error);

			if (!guildMember.roles.cache.has('842549363153829928'))
				await guildMember.roles.add('842549363153829928').catch(console.error);

			for (const [id, reaction] of colorMsg.reactions.cache) {
				if (reaction.users.cache.has(user.id) && reaction.emoji.id != colorRole.emote)
					await reaction.users.remove(user.id)
			}
		});

		collector.on('remove', async (reaction, user) => {
			const guildMember = await guild.members.fetch(user.id);

			const removeRole = COLOR_ROLES.find(a => a.emote == reaction.emoji.id);

			await guildMember.roles.remove(removeRole.role)
				.catch(console.error);
		});

		const embed = {
			color: 0xb4327d,
			thumbnail: {
				url: guild.iconURL({ extension: 'png' })
			},
			title: 'Color Roles',
			description: 'Select a sparkly custom color role!\n',
			fields: []
		};

		const options = [{ label: 'None', value: 'none', emoji: '❌' }];

		for (const role of COLOR_ROLES) {
			embed.fields.push({
				inline: true,
				value: '‎ ',
				name: `${client.emojis.cache.get(role.emote)} ${role.name}`
			});

			options.push({ label: role.name, value: role.name, emoji: role.emote });

			// colorMsg.react(role.emote).catch(console.error);
		}

		const row = new ActionRowBuilder()
			.addComponents(
				new SelectMenuBuilder()
					.setCustomId('colorRole')
					.setPlaceholder('Select color role')
					.addOptions(options),
			);

		colorMsgObj = { embeds: [embed], components: [row] };

		await colorMsg.edit(colorMsgObj);
	});

	client.on('guildMemberRemove', async member => {
		if (member.guild.id != config.mainGuild) {
			return;
		}

		const reclaimable = await db.get(`reclaimroles_${member.id}`) ?? [];
		const reclaimroles = [...reclaimable, ...member.roles.cache.keys()];

		db.set(`reclaimroles_${member.id}`, reclaimroles).catch(helper.error);
	});

	process.on('message', message => {
		if (message === 'reloadCommands')
			reloadCommands();
	});
}

main();
