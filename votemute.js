const helper = require('./helper.js');

const DURATION = 180_000;
const MUTE_LENGTH = 16 * 60 * 60 * 1000;
const MINUTES_BETWEEN = 30;
const ROLE_REQUIRED = "707457249265582120";
const CHANNEL_ALLOWED = "756423244013109248";

const UNMUTABLE = [
    "692755790582513685", // Robot Lea
    "683977589575909402", // Robots
    "682878669198458926", // Mods
    "707457249265582120", // Heart,
];

module.exports = {
    call: async obj => {
        const { interaction, db, client } = obj;

        if (interaction.channel.id != CHANNEL_ALLOWED) {
            return await interaction.reply({
                content: "You can't do this in the cuwwent channel, try <#756423244013109248>",
                ephemeral: true
            });
        }

        if (!interaction.member.roles.cache.has(ROLE_REQUIRED)) {
            return await interaction.reply({
                content: "You don't have permissions to use this.",
                ephemeral: true
            });
        }

        const lastVote = await db.get(`lastvote_${interaction.user.id}`) || 0;

        if (Date.now() - lastVote < MINUTES_BETWEEN * 60 * 1000) {
            return await interaction.reply({
                content: `You can only start one vote mute per ${MINUTES_BETWEEN} minutes.`,
                ephemeral: true
            });
        }

        const member = interaction.options.getMember('member');
        const reason = interaction.options.getString('reason');

        if (member.roles.cache.hasAny(...UNMUTABLE)) {
            return await interaction.reply({
                content: "You can't vote to mute this user.",
                ephemeral: true
            });
        }

        const row = new MessageActionRow()
			.addComponents(
				new MessageButton()
					.setCustomId('yes')
					.setLabel('Yes')
					.setStyle('SUCCESS'),
                new MessageButton()
					.setCustomId('no')
					.setLabel('No')
					.setStyle('DANGER'),
			);

        let description = `Should ${memberNicknameMention(member.id)} be muted?`;
        description += '\n**3x** Yes required, **1x** No to prevent the mute.';

        const yesVotes = new Set();
        const noVotes = new Set();

        const fields = [
            {
                name: 'Reason',
                value: reason
            }, {
                name: 'Yes',
                value: yesVotes.size.toString(),
                inline: true
            }, {
                name: 'No',
                value: noVotes.size.toString(),
                inline: true
            }
        ];

        const embed = {
            description,
            fields,
            footer: {
                text: `Time left: ${DURATION / 1000} Seconds`
            }
        };

        await interaction.reply({
            embeds: [embed],
            components: [row]
        });

        await db.set(`lastvote_${interaction.user.id}`, Date.now())

        const reply = await interaction.fetchReply();
        const collector = reply.createMessageComponentCollector({ time: DURATION });

        const startDate = Date.now();

        const updateInterval = setInterval(async () => {
            const timeLeft = (DURATION - (Date.now() - startDate)) / 1000;

            embed.footer.text = `Time left: ${Math.ceil(Math.max(0, timeLeft))} Seconds`;

            embed.fields[1].value = yesVotes.size.toString();
            embed.fields[2].value = noVotes.size.toString();

            await interaction.editReply({
                embeds: [embed],
                components: [row]
            });
        }, 5000);

        collector.on('collect', async i => {
            const { id } = i.user;

            if (!i.member.roles.cache.has(ROLE_REQUIRED)) {
                return await i.reply({
                    content: "How?",
                    ephemeral: true
                });
            }

            let content;

            if (i.customId == 'yes') {
                if (yesVotes.has(id)) {
                    yesVotes.delete(id);
                    content = 'You have removed your **Yes** vote.';
                } else {
                    yesVotes.add(id);

                    if (noVotes.has(id)) {
                        noVotes.delete(id);
                        content = 'You have changed your vote to **Yes**.'
                    } else {
                        content = 'You have voted **Yes**!'
                    }
                }
            } else if (i.customId == 'no') {
                if (noVotes.has(id)) {
                    noVotes.delete(id);
                    content = 'You have removed your **No** vote.'
                } else {
                    noVotes.add(id);

                    if (yesVotes.has(id)) {
                        yesVotes.delete(id);
                        content = 'You have changed your vote to **No**.'
                    } else {
                        content = 'You have voted **No**.'
                    }
                }
            }

            return await i.reply({
                content,
                ephemeral: true
            });
        });

        collector.on('end', async () => {
            clearInterval(updateInterval);

            embed.fields[1].value = yesVotes.size.toString();
            embed.fields[2].value = noVotes.size.toString();
            delete embed.footer;

            if (noVotes.size >= 1) {
                embed.color = 0xfc5555;
                embed.description = `Vote to mute ${memberNicknameMention(member.id)} failed (1 No vote).`
            } else if (yesVotes.size >= 3) {
                await rest.patch(Routes.guildMember(interaction.guild.id, member.id), {
                    body: { 
                        communication_disabled_until: new Date(Date.now() + MUTE_LENGTH).toISOString()
                    }, headers: {
                        'X-Audit-Log-Reason': `Vote Mute: ${reason}`
                    }
                });

                const user = await client.users.fetch('137953476880629760');

                client.users.fetch('137953476880629760').then(user => {
                    user.send({
                        content: `Server member ${userMention(member.id)} has been successfully vote-muted. (Reason: ${reason})`
                    });
                }).catch(console.error);

                embed.color = helper.mainColor;
                embed.description = `Successfully muted ${memberNicknameMention(member.id)}.`;


            } else {
                embed.color = 0xfc5555;
                embed.description = `Vote to mute ${memberNicknameMention(member.id)} failed (Not enough votes).`
            }

            await interaction.editReply({
                embeds: [embed],
                components: []
            });
        });
    }
}
