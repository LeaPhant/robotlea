const helper = require('../helper.js');

module.exports = {
    command: 'jam',
    argsRequired: 0,
    call: obj => {
        if(Math.random() < 0.004){
            return { content: "https://tenor.com/view/14037372" };
        }

        return { content: "<a:catJAM:791080660903264256>" };
    }
};
