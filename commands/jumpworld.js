const axios = require('axios');
const helper = require('../helper.js');
const cheerio = require('cheerio');

const emojipedia = axios.create({
    baseURL: 'https://emojipedia.org',
    responseType: 'document'
})

module.exports = {
    command:  ['jw', 'jumpworld'],
    description: "Look up a player's Timolia JumpWorld stats.",
    argsRequired: 1,
    usage: '<ign>',
    example: {
        run: "jumpworld leaphant",
        result: "Returns JumpWorld stats for LeaPhant."
    },
    call: async obj => {
            const { argv, msg } = obj;

        try{
            const response = await axios.get(`https://www.timolia.de/en/stats/${argv[1]}`);

            const $ = cheerio.load(response.data);

            const username = $('#playername').text();
            let description = "";

            const uuid = $('.row.text-center.center-block > img').attr('src').split("/").pop();

            const fields = [];

            $("ul.list-group li").each(function(){
                if($(this).find('.stat-header').text() != 'JumpWorld')
                    return;

                $(this).find("div tbody tr").each(function(){
                    const name = $(this).find('td:nth-child(1)').text();
                    const value = $(this).find('td.align-right').text();

                    fields.push({ name, value });
                });
            });

            const passed = fields.find(a => a.name == 'Passed parkours of the day').value;
            const total = fields.find(a => a.name == 'Total parkours of the day').value;

            const completion = Number(passed) / Number(total);

            fields.splice(
                fields.findIndex(a => a.name == 'Passed parkours of the day'), 2,
                { name: 'Parkours of the day', value: `${passed} / ${total}` },
                { name: 'Completion', value: `${(completion * 100).toFixed(2)}%` }
            );

            for(const row of fields){
                description += `**${row['name']}**: ${row['value']}\n`;
            }

            const embed = {
                url: `https://www.timolia.de/en/stats/${username}`,
                title: `${username} – JumpWorld`,
                description,
                thumbnail: {
                    url: `https://minotar.net/helm/${uuid}/256`
                }
            };

            return { embeds: [embed] };
        }catch(e){
            console.error(e);
            throw "Error requesting stats.";
        }
    }
};
