const helper = require('../helper.js');
const config = require('../config.json');
const { Duration, DateTime } = require('luxon');
const _ = require('lodash');
const { ActivityType } = require('discord.js');
const fetch = require('node-fetch');

const LASTFM_BASE = "http://ws.audioscrobbler.com/2.0/";

const lastfmRequest = async (method, user) => {
    const params = new URLSearchParams({
        api_key: config.lastfmApiKey,
        format: 'json',
        method,
        user
    }).toString();

    const url = new URL(`?${params}`, LASTFM_BASE);

    const response = await fetch(url);

    return response.json();
}

const parseAsset = (assetUrl, applicationId) => {
    if (assetUrl == null) {
        return null;
    }

    const type = assetUrl.split(':')[0];

    switch (type) {
        case 'spotify':
            return `https://i.scdn.co/image/${assetUrl.split(':').pop()}`;
        case 'mp':
            const s = assetUrl.split('/');
            const protocol = s[2];

            return `${protocol}://${s.slice(3).join('/')}`;
        default:
            if (applicationId == null) {
                return null;
            }

            return `https://cdn.discordapp.com/app-assets/${applicationId}/${assetUrl}.png`;
    }
}

module.exports = {
    command: ['np', 'presence'],
    argsRequired: 0,
    call: async obj => {
        const { argv, guildId, msg, responseMsg, endEmitter } = obj;

	let member = msg?.member;

	if (argv.length > 1) {
        let mention = argv[1];

        if (mention.startsWith('<@') && mention.endsWith('>')) {
            mention = mention.slice(2, -1);
    
            if (mention.startsWith('!')) {
                mention = mention.slice(1);
            }
        }

	    member = msg.guild.members.cache.get(mention);
	}

    const activities = member?.presence?.activities ?? [];

    const embeds = [];

    for(const presence of activities){
        if(presence.type == 4)
            continue;

        const { name, details, state, assets } = presence;

        let authorName = member.user.username;

        if(member !== null && member.nickname !== null)
            authorName = member.nickname;

        let embed = {
            color: 0xb4327d,
            author: {
                name: authorName,
                icon_url: member.user.avatarURL()
            }
        };

        if(presence.name == 'Spotify' && presence.applicationID == null){
            console.log(presence);

            let title = details;
            let artist = presence.state;
            let album = assets;
            let album_name = album.largeText;
            let album_cover = album.largeImage.split(':');
            let track_url = `https://open.spotify.com/track/${presence.syncId}`;
            let username = msg.author.username;

            const { start, end } = presence.timestamps;

            const total = Duration.fromObject({ milliseconds: end.getTime() - start.getTime() });
            const current = Duration.fromObject({ milliseconds: Date.now() - start.getTime() });

            if(msg.member !== null && msg.member.nickname !== null)
                username = member.nickname;

            if(album.largeImage.length > 0){
                album_cover = parseAsset(album.largeImage);

                embed = Object.assign(embed, {
                    color: 1947988,
                    footer: {
                        icon_url: "https://cdn.discordapp.com/attachments/572429763700981780/807009451173216277/favicon-1.png",
                        text: `Spotify${helper.sep}Listening right now`
                    },
                    thumbnail: {
                        url: album_cover
                    },
                    title: `**${artist}** – ${title}`,
                    description: `Album: **${album_name}** – \`${current.toFormat('mm:ss')}\`/\`${total.toFormat('mm:ss')}\``,
                    url: track_url
                });

                embeds.push(embed);
            }

            continue;
        }else if(assets == null){
            let description = `${_.capitalize(ActivityType[presence.type])} **${presence.name.replaceAll('*', '\\*')}**`;

            if(presence?.timestamps != null){
                const { start } = presence.timestamps;

                if(start != null)
                    description += ` since <t:${Math.round(start.getTime() / 1000)}:R>.`;
            }

            embed = Object.assign(embed, { description });

            embeds.push(embed);
            continue;
        }

        embed.description = `${details}\n${state}`;

        const largeImageUrl = parseAsset(assets.largeImage, presence.applicationId);
        const smallImageUrl = parseAsset(assets.smallImage, presence.applicationId);

        embed.thumbnail = {
            url: largeImageUrl
        };

        embed.footer = {
            icon_url: smallImageUrl,
            text: `${name}${helper.sep}${assets.largeText}`
        };

        embeds.push(embed);
    }

        if(embeds.length == 0 && argv.length < 2 && !config.lastfmApiKey){
            return "You don't have any active presences";
        }

        if (embeds.length == 0) {
            try {
                const { user } = await lastfmRequest('user.getinfo', argv[1]);
                const { recenttracks } = await lastfmRequest('user.getRecentTracks', argv[1]);
    
                if(recenttracks.track.length == 0){
                    return `This user hasn't listened to anything yet`;
                }else{
                    let listening_text = "";
                    let track = recenttracks.track[0];
        
                    if(track["@attr"] != undefined && track["@attr"].nowplaying == 'true'){
                        listening_text = 'Listening right now';
                    }else{
                        listening_text = `Listened ${DateTime.fromSeconds(Number(track.date.uts)).toRelative()}`;
                    }
        
                    embed = {
                        color: 13959168,
                        footer: {
                            icon_url: "https://cdn.discordapp.com/attachments/532034792804581379/591679254656319556/lastfm-1.png",
                            text: `Last.fm${helper.sep}${listening_text}`
                        },
                        thumbnail: {
                            url: track.image[2]["#text"]
                        },
                        title: `**${track.artist["#text"]}** – ${track.name}`,
                        url: track.url,
                        author: {
                            name: user.name,
                            url: user.url,
                            icon_url: user.image["0"]["#text"]
                        }
                    };
                    
                    if(track.album["#text"].length > 0)
                        embed.description = `_ _\nAlbum: **${track.album["#text"]}**`
        
                    embeds.push(embed);
                }
            } catch(e) {
                helper.error(e);
                return "Failed returning last.fm track info.";
            }
        }

        let message = responseMsg;

        if(embeds.length > 1){
            for(const [index, embed] of embeds.entries()){
                if(embed.footer == null){
                    embed.footer = { text: `(${index + 1}/${embeds.length})` };
                }else{
                    embed.footer.text += `${helper.sep}(${index + 1}/${embeds.length})`;
                }
            }
        }

        if(responseMsg)
            await responseMsg.edit({ embed: embeds[0] });
        else
            message = await msg.channel.send({ embeds: [embeds[0]] });

        if(embeds.length > 1){
            let currentEmbed = 0;

            const collector = message.createReactionCollector({ filter:
                (reaction, user) => user.bot === false && user.id == msg.author.id, idle: 120 * 1000 }
            );

            collector.on('collect', async (reaction, user) => {
                reaction.users.remove(user.id).catch(console.error);

                if(reaction._emoji.name == '⬅️'){
                    currentEmbed = Math.max(0, currentEmbed - 1);
                }else if(reaction._emoji.name == '➡️'){
                    currentEmbed = Math.min(embeds.length - 1, currentEmbed + 1);
                }

                await message.edit({ embeds: [embeds[currentEmbed]] });
            });

            collector.on('end', () => {
                message.reactions.removeAll().catch(console.error);
            });

            endEmitter.once(`end-${guildId}_${message.channel.id}_${message.id}`, () => {
                collector.stop();
            });

            await message.react('⬅️');
            await message.react('➡️');
        }

        return message;
    }
};
