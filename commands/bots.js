module.exports = {
    command: ['bots', 'matrix', 'bot'],
    argsRequired: 0,
    call: obj => {
        const { msg } = obj;
        return { embeds: [{
            title: "What's up with all those bots?",
            description: "This Discord server is connected to an *almost* identical Matrix space, chat messages are mirrored over a webhook. Matrix is an alternative chat protocol that is open-source and decentralized – you can join our Matrix space via this link: https://matrix.to/#/#sky:m.lea.moe.",
            color: 0xb4327d,
            thumbnail: {
                url: msg.guild.iconURL({ dynamic: true })
            }
        }] };
    }
};
