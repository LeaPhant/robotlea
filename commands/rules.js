const helper = require('../helper.js');
const rules = [
    {
        title: "Be nice to each other. 🌹",
        description: "**Any hate speech is prohibited** and will get you banned pretty quickly. This includes using ethnic/homophobic/transphobic slurs no matter the context. Abusing terms describing disablities or minorities in a derogatory context is also unwelcome."
    },
    {
        title: "No NSFW content.",
        description: "No NSFW pictures, videos, memes or language."
    },
    {
        title: "Try to mostly speak English.",
        description: "This is to ensure making the server as accessible as possible to everyone."
    },
    {
        title: "Don't leak personal info.",
        description: "Don't send personal stuff like full names, phone numbers, (e-mail) addresses and the like, **even if it's faked**. This applies to both other people's and your own info."
    },
    {
        title: "Don't advertise.",
        description: "Don't advertise other Discord servers or send ref links and similar spam. This rule applies to both the server and DMs with other server members."
    },
    {
        title: "Mark sensitive content with spoilers.",
        description: "If you want to share anything that might include sensitive content (hate speech, scary images etc.) please put it in spoiler tags and add a content warning (e.g. cw transphobia ||https://twitter.com/jk_rowling||)"
    }
];

module.exports = {
    command: ['rules', 'rule'],
    argsRequired: 0,
    call: obj => {
        const { argv, msg } = obj;

        let rule = {
            title: "Rules",
            color: 0xb4327d,
            description: "Please read the <#680404415001526488> before participating in this server.",
            thumbnail: {
                url: msg.guild.iconURL()
            }
        }

        if(argv.length > 1){
            const ruleN = Number(argv[1]);

            if(ruleN >= 1 && ruleN <= rules.length){
                rule = Object.assign(rule, rules[Number(argv[1]) - 1]);

                rule.description += "\n\nFor further info check <#680404415001526488>.";
            }
        }

        return { embeds: [rule] };
    }
};
